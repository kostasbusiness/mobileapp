﻿using Microsoft.Maui.Controls;
using System.Collections.ObjectModel;
using MobileApp.Models;
using MobileApp.Services;

namespace MobileApp
{
    public partial class CourseDetailsPage : ContentPage
    {
        private readonly DatabaseService _databaseService = new DatabaseService();
        private readonly Course _currentCourse;
        public ObservableCollection<Assessment> Assessments { get; } = new ObservableCollection<Assessment>();

        public CourseDetailsPage(Course course)
        {
            InitializeComponent();
            _currentCourse = course;
            Title = $"{_currentCourse.Name} Details";
            assessmentsCollectionView.ItemsSource = Assessments;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await LoadAssessments();
        }

        private async Task LoadAssessments()
        {
            Assessments.Clear();
            var assessmentsList = await _databaseService.GetAssessmentsByCourseIdAsync(_currentCourse.Id);
            foreach (var assessment in assessmentsList)
            {
                Assessments.Add(assessment);
            }
        }

        private async void CreateAssessment_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddAssessmentPage(_currentCourse));
        }
        private async void EditAssessment_Clicked(object sender, EventArgs e)
        {
            if (sender is Button button && button.BindingContext is Assessment assessment)
            {
                await Navigation.PushAsync(new EditAssessmentPage(assessment));
            }
        }
        private async void DeleteAssessment_Clicked(object sender, EventArgs e)
        {
            if (sender is Button button && button.BindingContext is Assessment assessment)
            {
                bool confirmed = await DisplayAlert("Confirm", "Are you sure you want to delete this assessment?", "Yes", "No");
                if (confirmed)
                {
                    await _databaseService.DeleteAssessmentAsync(assessment.Id);
                    Assessments.Remove(assessment);
                }
            }
        }
    }
}
