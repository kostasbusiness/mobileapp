﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SQLite;
using System.Threading.Tasks;
using MobileApp.Models;


namespace MobileApp.Services
{
    public class DatabaseService
    {
        private readonly SQLiteAsyncConnection _database;

        public DatabaseService()
        {
            string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "MobileAppDatabase.db3");
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Term>().Wait();
            _database.CreateTableAsync<Course>().Wait();
            _database.CreateTableAsync<Assessment>().Wait();
        }
        public async Task AddTermAsync(Term term)
        {
            await _database.InsertAsync(term);
        }

        public Task<List<Term>> GetTermsAsync()
        {
            return _database.Table<Term>().ToListAsync();
        }
        public async Task DeleteTermAsync(Term term)
        {
            await _database.DeleteAsync(term);
        }

        public async Task UpdateTermAsync(Term term)
        {
            await _database.UpdateAsync(term);
        }
        public async Task AddCourseAsync(Course course)
        {
            await _database.InsertAsync(course);
        }
        public async Task<List<Course>> GetCoursesByTermIdAsync(int termId)
        {
            return await _database.Table<Course>().Where(c => c.TermId == termId).ToListAsync();
        }
        public async Task DeleteCourseAsync(Course course)
        {
            await _database.DeleteAsync(course);
        }
        public async Task UpdateCourseAsync(Course course)
        {
            await _database.UpdateAsync(course);
        }
        public Task<List<Course>> GetCoursesAsync()
        {
            return _database.Table<Course>().ToListAsync();
        }

        public Task<List<Assessment>> GetAssessmentsAsync()
        {
            return _database.Table<Assessment>().ToListAsync();
        }
        public async Task<List<Assessment>> GetAssessmentsByCourseIdAsync(int courseId)
        {
            return await _database.Table<Assessment>().Where(a => a.CourseId == courseId).ToListAsync();
        }
        public async Task AddAssessmentAsync(Assessment assessment)
        {
            await _database.InsertAsync(assessment);
        }
        public async Task DeleteAssessmentAsync(int assessmentId)
        {
            var assessmentToDelete = await _database.FindAsync<Assessment>(assessmentId);
            if (assessmentToDelete != null)
            {
                await _database.DeleteAsync(assessmentToDelete);
            }
        }
        public async Task UpdateAssessmentAsync(Assessment assessment)
        {
            await _database.UpdateAsync(assessment);
        }








        public async Task SeedDatabaseAsync()
        {
            // Check if any terms exist
            var termsExist = await _database.Table<Term>().CountAsync() > 0;
            if (!termsExist)
            {
                // Seed a new term
                var newTerm = new Term
                {
                    Title = "2023 Term 1",
                    StartDate = new DateTime(2023, 1, 1),
                    EndDate = new DateTime(2023, 12, 31),
                };
                await _database.InsertAsync(newTerm);

                // Assuming the ID is auto-set upon insertion
                int termId = newTerm.Id;

                // Seed courses and assessments
                for (int i = 1; i <= 6; i++)
                {
                    var newCourse = new Course
                    {
                        TermId = termId,
                        Name = $"Course {i}",
                        StartDate = new DateTime(2023, 1, 1),
                        EndDate = new DateTime(2023, 12, 31),
                        Status = "In Progress",
                        InstructorName = "Anika Patel",
                        InstructorPhone = $"555-123-4567",
                        InstructorEmail = $"anika.patel@strimeuniversity.edu",
                        Notes = $"Notes for Course {i}"
                    };
                    await _database.InsertAsync(newCourse);

                    // Assuming the ID is auto-set upon insertion
                    int courseId = newCourse.Id;

                    // Seed an objective and a performance assessment for each course
                    var objectiveAssessment = new Assessment
                    {
                        CourseId = courseId,
                        Name = $"Objective Assessment for Course {i}",
                        Type = "Objective",
                        StartDate = new DateTime(2023, 6, 1),
                        EndDate = new DateTime(2023, 6, 2),
                    };
                    await _database.InsertAsync(objectiveAssessment);

                    var performanceAssessment = new Assessment
                    {
                        CourseId = courseId,
                        Name = $"Performance Assessment for Course {i}",
                        Type = "Performance",
                        StartDate = new DateTime(2023, 6, 15),
                        EndDate = new DateTime(2023, 6, 16),
                    };
                    await _database.InsertAsync(performanceAssessment);
                }
            }
        }

    }
}
