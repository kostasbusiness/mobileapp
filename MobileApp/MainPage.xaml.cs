﻿using Microsoft.Maui.Controls;
using System;
using MobileApp.Models;
using MobileApp.Services;
using System.Collections.ObjectModel;
using Plugin.LocalNotification;
#if ANDROID
using Android.App;
using Android.OS;
using Android.Content;
using Android.Provider;
#endif

namespace MobileApp

{
    public partial class MainPage : ContentPage
    {
        private readonly DatabaseService _databaseService;
        public ObservableCollection<Term> Terms { get; set; }
        private static bool _onlyOnce = false;

        public MainPage()
        {
            InitializeComponent();
            _databaseService = new DatabaseService();
            Terms = new ObservableCollection<Term>();
            termsCollectionView.ItemsSource = Terms;
        }

        public async void NotificationHandler()
        {
            if (await LocalNotificationCenter.Current.AreNotificationsEnabled() == false)
            {
                await LocalNotificationCenter.Current.RequestNotificationPermission();
            }

#if ANDROID
            if (OperatingSystem.IsAndroidVersionAtLeast(31))
            {
                var context = Android.App.Application.Context;
                var intent = new Intent(Android.Provider.Settings.ActionRequestScheduleExactAlarm);
                intent.SetData(Android.Net.Uri.Parse("package:" + context.PackageName));
                intent.AddFlags(ActivityFlags.NewTask);
                var alarmManager = context.GetSystemService(Context.AlarmService) as AlarmManager;
        if (!(alarmManager?.CanScheduleExactAlarms() ?? false))
        {
            context.StartActivity(intent);
        }
            }
#endif
        }



        protected override async void OnAppearing()
        {
            NotificationHandler();
            await CheckAndNotifyForToday();
            base.OnAppearing();
            var termsList = await _databaseService.GetTermsAsync();
            Terms.Clear();
            foreach (var term in termsList)
            {
                Terms.Add(term);
            }
        }

        private async void Edit_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var term = button?.BindingContext as Term;
            if (term != null)
            {
                await Navigation.PushAsync(new EditTermPage(term));
            }
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var term = button?.BindingContext as Term;
            if (term != null && await DisplayAlert("Confirm Delete", "Are you sure you want to delete this term?", "Yes", "No"))
            {
                await _databaseService.DeleteTermAsync(term);
                Terms.Remove(term);
            }
        }

        private async void OnAddTermClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddTermPage());
        }

        private async void OnTermSelected(object sender, SelectionChangedEventArgs e)
        {
            var selectedTerm = e.CurrentSelection.FirstOrDefault() as Term;
            if (selectedTerm != null)
            {
                await Navigation.PushAsync(new TermDetailsPage(selectedTerm));
                ((CollectionView)sender).SelectedItem = null;
            }
        }


        public async Task CheckAndNotifyForToday()
        {
            if (_onlyOnce)
            {
                return;
            }

            var today = DateTime.Today;
            var alertMessage = new System.Text.StringBuilder();

            //  Terms
            var allTerms = await _databaseService.GetTermsAsync();
            foreach (var term in allTerms)
            {
                if (term.StartDate.Date == today || term.EndDate.Date == today)
                {
                    alertMessage.AppendLine($"Term '{term.Title}' {(term.StartDate.Date == today ? "starts" : "ends")} today.");
                }
            }

            //  Courses
            var allCourses = await _databaseService.GetCoursesAsync();
            foreach (var course in allCourses)
            {
                if (course.StartDate.Date == today || course.EndDate.Date == today)
                {
                    alertMessage.AppendLine($"Course '{course.Name}' {(course.StartDate.Date == today ? "starts" : "ends")} today.");
                }
            }

            //  Assessments
            var allAssessments = await _databaseService.GetAssessmentsAsync();
            foreach (var assessment in allAssessments)
            {
                if (assessment.StartDate.Date == today || assessment.EndDate.Date == today)
                {
                    alertMessage.AppendLine($"Assessment '{assessment.Name}' {(assessment.StartDate.Date == today ? "starts" : "ends")} today.");
                }
            }

            if (alertMessage.Length > 0)
            {
                SendNotification("Today's Alerts", alertMessage.ToString());
            }
            _onlyOnce = true;
        }
        private void SendNotification(string title, string message)
        {
            var notification = new NotificationRequest
            {
                NotificationId = new Random().Next(),
                Title = title,
                Description = message,
                Schedule = new NotificationRequestSchedule { NotifyTime = DateTime.Now }
            };

            LocalNotificationCenter.Current.Show(notification);
        }

    }

}
