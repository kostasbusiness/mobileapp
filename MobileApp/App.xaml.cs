﻿using Plugin.LocalNotification;
using Plugin.LocalNotification.EventArgs;
using MobileApp.Models;
using MobileApp.Services;
using System.Collections.ObjectModel;
using Microsoft.Maui.Controls;


namespace MobileApp
{
    public partial class App : Application
    {
        private DatabaseService databaseService;

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new SplashPage());
            InitializeAppAsync().ConfigureAwait(false);
            LocalNotificationCenter.Current.NotificationActionTapped += OnNotificationActionTapped;
        }

        private async Task InitializeAppAsync()
        {
            var databaseService = new DatabaseService();
            await databaseService.SeedDatabaseAsync();

            MainThread.BeginInvokeOnMainThread(() =>
            {
                MainPage = new NavigationPage(new MainPage());
            });
        }

        private void OnNotificationActionTapped(NotificationActionEventArgs e)
        {
            if (e.IsDismissed || e.IsTapped)
            {
            }
        }
    }
}