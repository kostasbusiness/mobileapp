﻿using Microsoft.Maui.Controls;
using System;
using System.Text.RegularExpressions;
using MobileApp.Models;
using MobileApp.Services;
using Plugin.LocalNotification;


namespace MobileApp
{
    public partial class AddCoursePage : ContentPage
    {
        private readonly DatabaseService _databaseService;
        private readonly Term _currentTerm;

        public AddCoursePage(Term currentTerm)
        {
            InitializeComponent();
            _databaseService = new DatabaseService();
            _currentTerm = currentTerm;
        }
        private bool IsNumbersAndDashesOnly(string str)
        {
            return Regex.IsMatch(str, @"^[0-9-]*$");
        }
        private async void SaveCourse_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(courseNameEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter a course name.", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(instructorNameEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter the instructor's name.", "OK");
                return;
            }

            if (statusPicker.SelectedItem == null)
            {
                await DisplayAlert("Validation Error", "Please select a course status.", "OK");
                return;
            }

            if (startDatePicker.Date >= endDatePicker.Date)
            {
                await DisplayAlert("Validation Error", "Start date must be before the end date.", "OK");
                return;
            }

            if (!IsValidEmail(instructorEmailEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter a valid email address.", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(instructorPhoneEntry.Text) || !IsNumbersAndDashesOnly(instructorPhoneEntry.Text))
            {
                await DisplayAlert("Validation Error", "Instructor phone must contain only numbers and dashes.", "OK");
                return;
            }


            var course = new Course
            {
                Name = courseNameEntry.Text,
                StartDate = startDatePicker.Date,
                EndDate = endDatePicker.Date,
                Status = statusPicker.SelectedItem.ToString(),
                InstructorName = instructorNameEntry.Text,
                InstructorPhone = instructorPhoneEntry.Text,
                InstructorEmail = instructorEmailEntry.Text,
                Notes = notesEditor.Text,
                TermId = _currentTerm.Id
            };

            await _databaseService.AddCourseAsync(course);
            await DisplayAlert("Success", "Course added successfully.", "OK");
            ScheduleCourseNotifications(course);
            await Navigation.PopAsync();

        }
        private void ScheduleCourseNotifications(Course course)
        {
          
            var consolidatedNotification = new NotificationRequest
            {
                NotificationId = GenerateNotificationIdForCourse(course),
                Title = $"Upcoming Course: {course.Name}",
                Description = $"The course '{course.Name}' is starting on {course.StartDate:d} and ending on {course.EndDate:d}.",
                Schedule = new NotificationRequestSchedule { NotifyTime = DateTime.Now }
            };
            LocalNotificationCenter.Current.Show(consolidatedNotification);
        }
        private int GenerateNotificationIdForCourse(Course course)
        {
            return course.Id * 1000; 
        }
 
        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                email = Regex.Replace(email, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                return Regex.IsMatch(email,
                      @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            var idn = new System.Globalization.IdnMapping();

            var domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Invalid domain name", domainName);
            }
            return match.Groups[1].Value + domainName;
        }
        private bool IsAllDigits(string str)
        {
            return str.All(char.IsDigit);
        }
    }
}
