﻿using Microsoft.Maui.Controls;
using System;
using MobileApp.Models;
using MobileApp.Services;

namespace MobileApp
{
    public partial class EditTermPage : ContentPage
    {
        private readonly DatabaseService _databaseService;
        private Term _currentTerm;

        public EditTermPage(Term term)
        {
            InitializeComponent();
            _databaseService = new DatabaseService();
            _currentTerm = term;
            PopulateFormData();
        }

        private void PopulateFormData()
        {
            titleEntry.Text = _currentTerm.Title;
            startDatePicker.Date = _currentTerm.StartDate;
            endDatePicker.Date = _currentTerm.EndDate;
        }

        private async void SaveChanges_Clicked(object sender, EventArgs e)
        {
            _currentTerm.Title = titleEntry.Text;
            _currentTerm.StartDate = startDatePicker.Date;
            _currentTerm.EndDate = endDatePicker.Date;

            await _databaseService.UpdateTermAsync(_currentTerm);
            await DisplayAlert("Success", "Term updated successfully.", "OK");
            await Navigation.PopAsync();
        }
    }
}