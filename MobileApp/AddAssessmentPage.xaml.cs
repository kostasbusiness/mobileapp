﻿using Microsoft.Maui.Controls;
using System;
using MobileApp.Models;
using MobileApp.Services;
using Plugin.LocalNotification;

namespace MobileApp
{
    public partial class AddAssessmentPage : ContentPage
    {
        private readonly DatabaseService _databaseService = new DatabaseService();
        private readonly Course _currentCourse;

        public AddAssessmentPage(Course currentCourse)
        {
            InitializeComponent();
            _currentCourse = currentCourse;
            typePicker.SelectedIndex = 0;
        }
        private void ScheduleCourseNotifications(Assessment newAssessment)
        {

            var consolidatedNotification = new NotificationRequest
            {
                NotificationId = GenerateNotificationIdForCourse(newAssessment),
                Title = $"Upcoming Term: {newAssessment.Name}",
                Description = $"The assessment '{newAssessment.Name}' is starting on {newAssessment.StartDate:d} and ending on {newAssessment.EndDate:d}.",
                Schedule = new NotificationRequestSchedule { NotifyTime = DateTime.Now }
            };
            LocalNotificationCenter.Current.Show(consolidatedNotification);
        }
        private int GenerateNotificationIdForCourse(Assessment newAssessment)
        {
            return newAssessment.Id * 1000;
        }
        private async void OnSaveClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(nameEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter an assessment name.", "OK");
                return;
            }

            if (typePicker.SelectedItem == null)
            {
                await DisplayAlert("Validation Error", "Please select an assessment type.", "OK");
                return;
            }

            if (startDatePicker.Date >= endDatePicker.Date)
            {
                await DisplayAlert("Validation Error", "The start date must be before the end date.", "OK");
                return;
            }

            var newAssessment = new Assessment
            {
                CourseId = _currentCourse.Id,
                Name = nameEntry.Text,
                Type = typePicker.SelectedItem.ToString(),
                StartDate = startDatePicker.Date,
                EndDate = endDatePicker.Date
            };

            await _databaseService.AddAssessmentAsync(newAssessment);
            await DisplayAlert("Success", "Assessment added successfully.", "OK");
            ScheduleCourseNotifications(newAssessment);
            await Navigation.PopAsync();
        }
    }
}