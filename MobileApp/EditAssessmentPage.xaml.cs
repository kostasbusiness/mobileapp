﻿using Microsoft.Maui.Controls;
using System;
using MobileApp.Models;
using MobileApp.Services;

namespace MobileApp
{
    public partial class EditAssessmentPage : ContentPage
    {
        private readonly DatabaseService _databaseService = new DatabaseService();
        private Assessment _currentAssessment;

        public EditAssessmentPage(Assessment assessment)
        {
            InitializeComponent();
            _currentAssessment = assessment;
            PopulateFormData();
        }

        private void PopulateFormData()
        {
            nameEntry.Text = _currentAssessment.Name;
            typePicker.SelectedItem = _currentAssessment.Type;
            startDatePicker.Date = _currentAssessment.StartDate;
            endDatePicker.Date = _currentAssessment.EndDate;
        }

        private async void OnSaveClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(nameEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter an assessment name.", "OK");
                return;
            }

            if (typePicker.SelectedItem == null)
            {
                await DisplayAlert("Validation Error", "Please select an assessment type.", "OK");
                return;
            }

            if (startDatePicker.Date >= endDatePicker.Date)
            {
                await DisplayAlert("Validation Error", "The start date must be before the end date.", "OK");
                return;
            }

            _currentAssessment.Name = nameEntry.Text;
            _currentAssessment.Type = typePicker.SelectedItem.ToString();
            _currentAssessment.StartDate = startDatePicker.Date;
            _currentAssessment.EndDate = endDatePicker.Date;

            await _databaseService.UpdateAssessmentAsync(_currentAssessment);
            await DisplayAlert("Success", "Assessment updated successfully.", "OK");
            await Navigation.PopAsync();
        }
    }
}
