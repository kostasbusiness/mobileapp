﻿using Microsoft.Maui.Controls;
using System;
using System.Threading.Tasks;
using MobileApp.Models;
using MobileApp.Services;
using System.Collections.ObjectModel;
using Microsoft.Maui.ApplicationModel;
using static System.Net.Mime.MediaTypeNames;


namespace MobileApp
{
    public partial class TermDetailsPage : ContentPage
    {
        private readonly Term _currentTerm;
        private readonly DatabaseService _databaseService = new DatabaseService();
        public ObservableCollection<Course> Courses { get; } = new ObservableCollection<Course>();

        public TermDetailsPage(Term term)
        {
            InitializeComponent();
            _currentTerm = term;
            PopulateTermDetails();
            coursesCollectionView.ItemsSource = Courses;
            coursesCollectionView.SelectionChanged += CoursesCollectionView_SelectionChanged;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await LoadCourses();
        }

        private async Task LoadCourses()
        {
            Courses.Clear();
            var coursesList = await _databaseService.GetCoursesByTermIdAsync(_currentTerm.Id);
            foreach (var course in coursesList)
            {
                Courses.Add(course);
            }
        }

        private async void ShareNotes_Clicked(object sender, EventArgs e)
        {
            if (sender is Button button && button.BindingContext is Course course)
            {
                var notes = string.IsNullOrWhiteSpace(course.Notes) ? "No notes available for this course." : course.Notes;

                await Share.Default.RequestAsync(new ShareTextRequest
                {
                    Text = notes,
                    Title = $"Notes for {course.Name}"
                });
            }
        }
        private void PopulateTermDetails()
        {
            titleLabel.Text = $"Term: {_currentTerm.Title}";
            startDateLabel.Text = $"Start Date: {_currentTerm.StartDate:d}";
            endDateLabel.Text = $"End Date: {_currentTerm.EndDate:d}";
        }

        private async void AddCourses_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddCoursePage(_currentTerm));
        }

        private async void EditCourse_Clicked(object sender, EventArgs e)
        {
            if (sender is Button button && button.BindingContext is Course course)
            {
                await Navigation.PushAsync(new EditCoursePage(course));
            }
        }

        private async void DeleteCourse_Clicked(object sender, EventArgs e)
        {
            if (sender is Button button && button.BindingContext is Course course)
            {
                bool confirmed = await DisplayAlert("Confirm", "Delete this course?", "Yes", "No");
                if (confirmed)
                {
                    await _databaseService.DeleteCourseAsync(course);
                    Courses.Remove(course);
                    await LoadCourses(); 
                }
            }
        }

        private async void CoursesCollectionView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedCourse = e.CurrentSelection.FirstOrDefault() as Course;
            if (selectedCourse != null)
            {
                await Navigation.PushAsync(new CourseDetailsPage(selectedCourse));
                ((CollectionView)sender).SelectedItem = null; 
            }
        }
    }
}
