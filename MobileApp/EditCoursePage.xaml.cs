﻿using Microsoft.Maui.Controls;
using System;
using MobileApp.Models;
using MobileApp.Services;
using System.Text.RegularExpressions;

namespace MobileApp
{
    public partial class EditCoursePage : ContentPage
    {
        private Course _currentCourse;
        private readonly DatabaseService _databaseService = new DatabaseService();

        public EditCoursePage(Course course)
        {
            InitializeComponent();
            _currentCourse = course;
            PopulateFormData();
        }

        private void PopulateFormData()
        {
            nameEntry.Text = _currentCourse.Name;
            startDatePicker.Date = _currentCourse.StartDate;
            endDatePicker.Date = _currentCourse.EndDate;
            statusPicker.SelectedItem = _currentCourse.Status;
            instructorNameEntry.Text = _currentCourse.InstructorName;
            instructorPhoneEntry.Text = _currentCourse.InstructorPhone;
            instructorEmailEntry.Text = _currentCourse.InstructorEmail;
            notesEditor.Text = _currentCourse.Notes;
        }
        private bool IsNumbersAndDashesOnly(string str)
        {
            return Regex.IsMatch(str, @"^[0-9-]*$");
        }
        private async void OnSaveClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(nameEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter a course name.", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(instructorNameEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter the instructor's name.", "OK");
                return;
            }

            if (statusPicker.SelectedItem == null)
            {
                await DisplayAlert("Validation Error", "Please select a course status.", "OK");
                return;
            }

            if (startDatePicker.Date >= endDatePicker.Date)
            {
                await DisplayAlert("Validation Error", "Start date must be before the end date.", "OK");
                return;
            }

            if (!IsValidEmail(instructorEmailEntry.Text))
            {
                await DisplayAlert("Validation Error", "Please enter a valid email address.", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(instructorPhoneEntry.Text) || !IsNumbersAndDashesOnly(instructorPhoneEntry.Text))
            {
                await DisplayAlert("Validation Error", "Instructor phone must contain only numbers and dashes.", "OK");
                return;
            }

            _currentCourse.Name = nameEntry.Text;
            _currentCourse.StartDate = startDatePicker.Date;
            _currentCourse.EndDate = endDatePicker.Date;
            _currentCourse.Status = statusPicker.SelectedItem.ToString();
            _currentCourse.InstructorName = instructorNameEntry.Text;
            _currentCourse.InstructorPhone = instructorPhoneEntry.Text;
            _currentCourse.InstructorEmail = instructorEmailEntry.Text;
            _currentCourse.Notes = notesEditor.Text;

            await _databaseService.UpdateCourseAsync(_currentCourse);
            await DisplayAlert("Success", "Course updated successfully.", "OK");
            await Navigation.PopAsync();
        }

        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }

            string pattern = @"^[^@\s]+@[^@\s]+\.[^@\s]+$";
            return Regex.IsMatch(email, pattern, RegexOptions.IgnoreCase);
        }

        private bool IsAllDigits(string str)
        {
            return str.All(char.IsDigit);
        }
    }
}
