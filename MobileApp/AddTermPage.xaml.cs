﻿using Microsoft.Maui.Controls;
using System;
using MobileApp.Models;
using MobileApp.Services;
using Plugin.LocalNotification;

namespace MobileApp
{
    public partial class AddTermPage : ContentPage
    {
        private readonly DatabaseService _databaseService;

        public AddTermPage()
        {
            InitializeComponent();
            _databaseService = new DatabaseService();
        }
        private void ScheduleCourseNotifications(Term term)
        {

            var consolidatedNotification = new NotificationRequest
            {
                NotificationId = GenerateNotificationIdForCourse(term),
                Title = $"Upcoming Term: {term.Title}",
                Description = $"The term '{term.Title}' is starting on {term.StartDate:d} and ending on {term.EndDate:d}.",
                Schedule = new NotificationRequestSchedule { NotifyTime = DateTime.Now }
            };
            LocalNotificationCenter.Current.Show(consolidatedNotification);
        }
        private int GenerateNotificationIdForCourse(Term term)
        {
            return term.Id * 1000;
        }
        private async void SaveTerm_Clicked(object sender, EventArgs e)
        {
            try
            {
                var term = new Term
                {
                    Title = titleEntry.Text,
                    StartDate = startDatePicker.Date, 
                    EndDate = endDatePicker.Date 
                };

                if (string.IsNullOrWhiteSpace(term.Title))
                {
                    await DisplayAlert("Error", "Please enter a term title.", "OK");
                    return;
                }
                if (term.StartDate >= term.EndDate)
                {
                    await DisplayAlert("Error", "The start date must be before the end date.", "OK");
                    return;
                }

                await _databaseService.AddTermAsync(term);

                await DisplayAlert("Success", "Term added successfully.", "OK");
                ScheduleCourseNotifications(term);

                await Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Failed to save the term. Please try again.", "OK");
            };
        }
    }
}
