﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileApp.Models
{
    public class Assessment
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int CourseId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; } 

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
